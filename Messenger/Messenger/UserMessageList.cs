﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Messenger
{
    class UserMessageList
    {
        public int Id { get; set; }
        public string Forname { get; set; }
        public string Surname { get; set; }
        public string LastMessage { get; set; }
    }
}