﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Messenger
{
    internal class ListViewAdapter : BaseAdapter<UserMessageList>
    {
        public List<UserMessageList> Users;
        private Context context;

        public ListViewAdapter(Context context, List<UserMessageList> users)
        {
            this.Users = users;
            this.context = context;
        }

        public override int Count
        {
            get
            {
                return Users.Count;
            }
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override UserMessageList this[int position]
        {
            get { return Users[position]; }
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View row = convertView;

            if (row == null)
            {
                row = LayoutInflater.From(context).Inflate(Resource.Layout.MessagesListViewRow, null, false);
            }

            TextView TextViewUserName = row.FindViewById<TextView>(Resource.Id.textViewUserName);
            TextView textViewLastMessage = row.FindViewById<TextView>(Resource.Id.textViewLastMessage);
            TextViewUserName.Text = Users[position].Forname + " " + Users[position].Surname;
            textViewLastMessage.Text = Users[position].LastMessage;

            return row;
        }
    }
}