﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Messenger
{
    class Message
    {
        public int Id { get; set; }
        public int IdUser { get; set; }
        public string Text { get; set; }

    }
}