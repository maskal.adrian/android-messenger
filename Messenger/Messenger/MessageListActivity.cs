﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Messenger
{
    [Activity(Label = "MessageList")]
    public class MessageListActivity : Activity
    {
        private List<UserMessageList> users;
        private ListView listView;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.MessageList);

            listView = FindViewById<ListView>(Resource.Id.listView);

            users = new List<UserMessageList>();

            users.Add(new UserMessageList { Id = 1, Forname = "Pepa", Surname = "Vocas", LastMessage = "Zdar" });
            users.Add(new UserMessageList { Id = 2, Forname = "Jozef", Surname = "Ocas", LastMessage = "Ahoj" });
            users.Add(new UserMessageList { Id = 3, Forname = "Jóža", Surname = "Vocásek", LastMessage = "Zdary" });
            users.Add(new UserMessageList { Id = 4, Forname = "Jozífek", Surname = "Ocásek", LastMessage = "Čus" });
            users.Add(new UserMessageList { Id = 5, Forname = "Pepek", Surname = "Koště", LastMessage = "Tě péro" });
            users.Add(new UserMessageList { Id = 6, Forname = "Pepíno", Surname = "Židle", LastMessage = "Hola" });
            users.Add(new UserMessageList { Id = 7, Forname = "Jožin", Surname = "Stůl", LastMessage = "Naaazdar" });
            users.Add(new UserMessageList { Id = 8, Forname = "Joseph", Surname = "Tail", LastMessage = "Hey" });
            users.Add(new UserMessageList { Id = 9, Forname = "Pepík", Surname = "Parník", LastMessage = "Ahojda" });

            ListViewAdapter adapter = new ListViewAdapter(this, users);

            listView.Adapter = adapter;
        }
    }
}