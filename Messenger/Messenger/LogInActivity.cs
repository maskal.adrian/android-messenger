﻿using System;
using Android.App;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;

namespace Messenger
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = true)]
    public class LogInActivity : Activity
    {
        private Button LogInBtn;
        private Button RegisterBtn;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.LogIn);

            LogInBtn = FindViewById<Button>(Resource.Id.LogInBtn);
            RegisterBtn = FindViewById<Button>(Resource.Id.RegisterBtn);

            LogInBtn.Click += LogInBtnClick;
            RegisterBtn.Click += RegisterBtnClick;
        }

        private void RegisterBtnClick(object sender, EventArgs e)
        {
            StartActivity(typeof(RegisterActivity));
            OverridePendingTransition(Resource.Animation.abc_slide_in_bottom, Resource.Animation.abc_slide_out_top);
        }

        private void LogInBtnClick(object sender, EventArgs e)
        {
            StartActivity(typeof(MessageListActivity));
            OverridePendingTransition(Resource.Animation.abc_slide_in_bottom, Resource.Animation.abc_slide_out_top);
        }

	}
}

