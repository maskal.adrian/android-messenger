﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Messenger
{
    [Activity(Label = "Register")]
    public class RegisterActivity : Activity
    {
        private Button Back;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.Register);

            Back = FindViewById<Button>(Resource.Id.buttonBack);

            Back.Click += ButtonBackClick;
        }

        protected void ButtonBackClick(object sender, EventArgs e)
        {
            StartActivity(typeof(LogInActivity));
        }
    }
}